# README #

This repository contains a database sample which is used for topology, workload and performance operations. 

the neo4j database version is 2.3.2 and installed on Ubuntu. 


#GUIDE for How to import this database#


1. Following the installation Mannul to deploy topology and persistence framework. 
2. Install a Neo4j database with version 2.3.2 of commnunity edtion with default directory. 
3. Execute command `service neo4j-service stop` to stop database service in a Terminal.
4. Execute command `cd /var/lib/neo4j/data/` to set to Neo4j database directory and download the database sample `graph.db.tar.gz` to this directory. 
5. Execute command `rm -rf graph.db` to clean the database. 
6. Execute command `tar -zxf graph.db.tar.gz` to unzip the sample. 
7. Execute command `sudo chmod -R 777 /var/lib/neo4j/data/` change the permission of the graph database directory. 
8. 7. Execute command `sudo service tomcat7 restart` to restart tomcat service.

Now the sample is deployed. 
